# Font Minify

Minify and create woff and woff 2 fonts from ttf files.

Create a /to-convert folder into the root, and put your ttf fonts inside it. Then run, node font-minify.js.
The converted and compressed file will be inside the /converted folder.