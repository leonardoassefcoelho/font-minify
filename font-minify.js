var Fontmin = require('fontmin');
var rename = require('gulp-rename');

var fontmin = new Fontmin()
    .src('to-convert/*.ttf')
    .dest('converted')
    .use(Fontmin.glyph({
    }))
    .use(Fontmin.ttf2woff({
        deflate: false
    }))
    .use(Fontmin.ttf2woff2({
    }))

fontmin.run();
